FROM php:7.4-apache

RUN usermod --non-unique --uid 1000 www-data
RUN groupmod --non-unique --gid 1000 www-data

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite

RUN chown -R www-data:www-data /var/www


RUN apt-get update
RUN apt-get install wget -y

#Instalando composer
ADD docker/install_composer.bash /var/www/install_composer.bash
RUN sh /var/www/install_composer.bash

#Instalando symfony-cli
ADD docker/install_symfony-cli.bash /var/www/install_symfony-cli.bash
RUN sh /var/www/install_symfony-cli.bash

## Installing git
RUN apt-get install -y git
CMD ["apache2-foreground"]