<?php

// validaciones
if (!isset($_POST['username']) || $_POST['username'] === '') {
    echo "Falta el nombre de Usuario";
    exit;
}
$username = $_POST['username'];

if (!isset($_POST['password']) || $_POST['password'] === '') {
    echo "Falta la contraseña del Usuario";
    exit;
}

$password = $_POST['password'];

if (!isset($_POST['age']) || $_POST['age'] === "") {
    echo "Falta la Edad del Usuario";
    exit;
}
$age = (int)$_POST['age'];


//lectura de usuarios

$registeredUsersFile = '/var/www/html/data/users.json';
if (file_exists($registeredUsersFile)) {
    $registeredUsersJson = file_get_contents($registeredUsersFile);
} else {
    file_put_contents($registeredUsersFile, '');
    $registeredUsersJson = '';
}


if ($registeredUsersJson === '') {
    $registeredUsers = []; //si está vacío no hay usuarios registrados
} else {
    $registeredUsers = json_decode($registeredUsersJson, true);
}

// persistencia de usuario

$updatedRegisteredUsers = array_merge(
    $registeredUsers,
    [
        [
            'username' => $username,
            'password' => $password,
            'age' => $age,
        ],
    ]
);
file_put_contents(
    $registeredUsersFile,
    json_encode(
        array_unique($updatedRegisteredUsers, SORT_REGULAR)
    )
);


echo "<br><br><br> Trazas:<br>";
var_dump($username, $password, $age);