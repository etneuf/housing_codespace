<?php declare(strict_types=1);

use Controller\RegistrationController;
use Service\Router;

require_once __DIR__.'/../vendor/autoload.php';



$router = new Router($_SERVER);


$router->add('/register', RegistrationController::class, 'register', 'POST');
$router->add('/register', RegistrationController::class, 'renderForm', 'GET');
$router->add('/user/13', RegistrationController::class, 'renderForm', 'GET');

$router->resolve();
