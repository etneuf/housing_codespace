<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita3c4d5f58dbb20f6e48fabb792c03e5a
{
    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/../..' . '/src',
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'Controller\\Base\\AbstractController' => __DIR__ . '/../..' . '/src/Controller/Base/AbstractController.php',
        'Controller\\RegistrationController' => __DIR__ . '/../..' . '/src/Controller/RegistrationController.php',
        'Entity\\User' => __DIR__ . '/../..' . '/src/Entity/User.php',
        'Repository\\UserRepository' => __DIR__ . '/../..' . '/src/Repository/UserRepository.php',
        'Service\\RegistrationInputValidator' => __DIR__ . '/../..' . '/src/Service/RegistrationInputValidator.php',
        'Service\\TemplateRenderer' => __DIR__ . '/../..' . '/src/Service/TemplateRenderer.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->fallbackDirsPsr4 = ComposerStaticInita3c4d5f58dbb20f6e48fabb792c03e5a::$fallbackDirsPsr4;
            $loader->classMap = ComposerStaticInita3c4d5f58dbb20f6e48fabb792c03e5a::$classMap;

        }, null, ClassLoader::class);
    }
}
