<?php declare(strict_types=1);

namespace Service;

use Entity\User;

class RegistrationInputValidator
{
    /** @var string[] */
    private array $errors;
    public function __construct(User $user)
    {
        $this->validate($user);
    }

    private function validate(User $user) {
        $this->errors = [];
        if ($user->getUsername() === '') {
            $this->errors[] =  "Falta el nombre de Usuario";
        }

        if ($user->getPassword() === '') {
            $this->errors[] =  "Falta la contraseña del Usuario";
        }

        if ($user->getAge() === "") {
            $this->errors[] =  "Falta la Edad del Usuario";
        }
    }

    public function isValid(): bool{
        return empty($this->errors);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}