<?php

namespace Service;

class Router
{
    private array $routes;

    private string $currentPath;

    private string $currentMethod;

    public function __construct(array $serverParams)
    {
        $this->currentPath = $serverParams['REQUEST_URI'];
        $this->currentMethod = $serverParams['REQUEST_METHOD'];
    }

    public function add(string $path, string $controller, string $action, string $method)
    {
        $this->routes[] = [
            'path' => $path,
            'controller' => $controller,
            'action' => $action,
            'method' => $method,
        ];
    }

    public function resolve()
    {
        foreach ($this->routes as $route) {
            if ($this->routeMatches($route['path'], $route['method'])) {
                $this->callControllersAction($route);

                return;
            }
        }
        echo "404 Not Found";
    }

    private function routeMatches(string $path, string $method){
        return $this->currentPath === $path && $this->currentMethod === $method;
    }

    private function callControllersAction($route): void
    {
        $controller = new $route['controller']();
        $action = $route['action'];
        $controller->$action($_GET, $_POST);
    }
}