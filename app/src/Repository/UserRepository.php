<?php declare(strict_types=1);

namespace Repository;

use Entity\User;

class UserRepository
{
    const REGISTERED_USERS_FILE = '/var/www/html/data/users.json';

    public function findAll(): array{
        if (file_exists(self::REGISTERED_USERS_FILE)) {
            $registeredUsersJson = file_get_contents(self::REGISTERED_USERS_FILE);
        } else {
            file_put_contents(self::REGISTERED_USERS_FILE, '');
            $registeredUsersJson = '';
        }

        if ($registeredUsersJson === '') {
            $registeredUsers = [];
        } else {
            $registeredUsers = json_decode($registeredUsersJson, true);
        }

        return $registeredUsers;
    }

    public function save(User $user): void{
        $registeredUsers = $this->findAll();

        $updatedRegisteredUsers = array_merge(
            $registeredUsers,
            [
                [
                    'username' => $user->getUsername(),
                    'password' => $user->getPassword(),
                    'age' => $user->getAge(),
                ],
            ]
        );

        file_put_contents(
            self::REGISTERED_USERS_FILE,
            json_encode(
                array_unique($updatedRegisteredUsers, SORT_REGULAR)
            )
        );
    }
}