<?php

namespace Controller;

use Repository\UserRepository;
use Controller\Base\AbstractController;
use DateTime;
use Entity\User;
use Service\RegistrationInputValidator;

class RegistrationController extends AbstractController
{
    public function register($getParams, $postParams){
        $user = new User($postParams['username'], $postParams['password'], (int)$postParams['age']);

        $registrationDataValidator = new RegistrationInputValidator($user);

        if(!$registrationDataValidator->isValid()){
            foreach ($registrationDataValidator->getErrors() as $error){
                echo $error."<br>";
            }
            exit;
        }

        $userRepository = new UserRepository();
        $userRepository->save($user);

        $users = $userRepository->findAll();

        $this->render('user_list_view.php', [
            'users' => $users, 'currentDate' => new DateTime()
            ]
        );
    }

    public function renderForm(array $getParams, array $postParams)
    {
        $this->render('registration_form.php', []);
    }
}

