<?php

namespace Controller\Base;

use Service\TemplateRenderer;

class AbstractController
{
    protected function render(string $templatePath, array $templateVars){
        $renderer = new TemplateRenderer();
        $renderer->render($templatePath, $templateVars);
    }
}