<?php declare(strict_types=1);

namespace Entity;

class User
{
    private string $username;
    private string $password;
    private int $age;

    public function __construct(string $username, string $password, int $age)
    {
        $this->username = $username;
        $this->password = $password;
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}